'use strict';

module.exports = function(grunt) {

  grunt.initConfig({

    watch: {
      options: {
          livereload: true
      },
      reload: {
        files : ['*', 'views/*']
      },
      scss: {
          files: ['stylesheets/*.scss', 'stylesheets/*/*.scss'],
          tasks: ['sass', 'cssmin'],
          options: {
              spawn: false
          }
      },
      img: {
          files: ['images/*', 'images/*/*'],
          tasks: ['imagemin'],
          options: {
              spawn: false
          }
      },
      js: {
          files: ['js/*', 'js/*.js','json/*','json/*.json'],
          tasks: ['uglify'],
          options: {
              spawn: false
          }
      }
    },
    browserSync: {
      dev: {
        options: {
          proxy: "localhost:8888",
          files: ['stylesheets/*', 'js/*.js', 'index.html'],
          watchTask: true
        }
      }
    },

    imagemin: {    
    dynamic: {                         // Another target
      files: [{
        expand: true,                  // Enable dynamic expansion
        cwd: 'images/',                   // Src matches are relative to this path
        src: ['*.{png,jpg,gif}'],   // Actual patterns to match
        dest: 'images/'                  // Destination path prefix
        }]
    }
    },

    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'stylesheets',
          src: ['*.scss'],
          dest: 'stylesheets',
          ext: '.css'
        }]
      }
    }, 

    cssmin: {
      compile: {
        files: {
         'stylesheets/style.min.css': ['stylesheets/style.css']
        } 
      }
    },

    uglify: {
      compile: {
        files: {
        'js/main.min.js': ['js/main.js']
        }
      }
    },

    });

    // Load tasks from NPM
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-notify');

    // Default task.
    grunt.registerTask('default', ['watch']);

};

