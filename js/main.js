(function($){$.fn.touchwipe=function(settings){var config={min_move_x:20,min_move_y:20,wipeLeft:function(){},wipeRight:function(){},wipeUp:function(){},wipeDown:function(){},preventDefaultEvents:true};if(settings)$.extend(config,settings);this.each(function(){var startX;var startY;var isMoving=false;function cancelTouch(){this.removeEventListener('touchmove',onTouchMove);startX=null;isMoving=false}function onTouchMove(e){if(config.preventDefaultEvents){e.preventDefault()}if(isMoving){var x=e.touches[0].pageX;var y=e.touches[0].pageY;var dx=startX-x;var dy=startY-y;if(Math.abs(dx)>=config.min_move_x){cancelTouch();if(dx>0){config.wipeLeft()}else{config.wipeRight()}}else if(Math.abs(dy)>=config.min_move_y){cancelTouch();if(dy>0){config.wipeDown()}else{config.wipeUp()}}}}function onTouchStart(e){if(e.touches.length==1){startX=e.touches[0].pageX;startY=e.touches[0].pageY;isMoving=true;this.addEventListener('touchmove',onTouchMove,false)}}if('ontouchstart'in document.documentElement){this.addEventListener('touchstart',onTouchStart,false)}});return this}})(jQuery);

(function() {

 // Preloader ===================================================

  var preloadImages = function(project, callback){


    var loadedImages = 0;

    // Loop through images
    $.each(project.images, function(i, val){

        // Setup new image object
        this.img = new Image()
        this.img.src = '../images/work/main/'+ val.src.substr(0, val.src.length-1) +'/'+ val.src + '.jpg';
        
        // Trigger callback on load of that img object
        this.img.onload = function(){

          loadedImages++;

          if(loadedImages == project.images.length){

            callback();
          } 
        };

    });

  };


  // Images ===================================================== 

  $.getJSON('js/work.json', function(data){

      var projects = data.projects;
      var currArr;

      $('.work-row > div').on('click', function(){

        var current = 0;

        var i = $(this).index('.work-row  div');
        currArr = projects[i];

        $('.loading').fadeTo(300, 1);

        preloadImages(currArr, function(){

          $('.loading').fadeTo(300, 0);

          // Callback used to wait for all images to be loaded before initisating
          $('.info-text').empty().html(currArr.copy);


          $('.pop').css({'background-image': 'url(../images/work/main/'+currArr.dir +'/'+ currArr.images[current].src+ '.jpg)'}).addClass('toggle');

          // if (currArr.images.length === 1){
          //   $('.pop').css({'background-image': 'url(../images/work/main/'+currArr.dir +'/'+ currArr.images[current].src+ '.jpg)'}).addClass('toggle');
          //   $('.pop').append('<div class="iframe-con"><iframe class="app-iframe" src="http://jackboyce.co.uk/react-quizapp/"></iframe></div>').addClass('toggle')
            
          // $(window).on('resize', function(){

          //   var gaga = $('.pop').height()/1500

          //   $('.app-iframe').css({'transform': 'scale('+gaga+')'})


          //   // $('.app-iframe').css{('height': )}
          // } else {
          //   $('.pop').css({'background-image': 'url(../images/work/main/'+currArr.dir +'/'+ currArr.images[current].src+ '.jpg)'}).addClass('toggle');
          // }

          
          if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)){

            $('.pop.toggle').touchwipe({

              wipeLeft: function() {    

                nextImg();
              },

              wipeRight: function() {         

                prevImg();
              },

              min_move_y: 20,
              preventDefaultEvents: true

            });

          } else {

            $('.pop-forward').click(function(){
              nextImg();
            });

            $('.pop-back').click(function(){
              prevImg();
            });

          }

          function nextImg(){
            current ++;
            current = (current >= currArr.images.length) ? 0 : current; 
            $('.pop').css('backgroundImage', 'url(../images/work/main/'+currArr.dir+'/'+currArr.images[current].src+'.jpg)');
          }

          function prevImg(){
            current--;
            current = (current < 0) ? currArr.images.length-1 : current;
            $('.pop').css('backgroundImage', 'url(../images/work/main/'+currArr.dir+'/'+currArr.images[current].src+'.jpg)');
          }

        });

        if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i) || currArr.images.length === 1){

          $('.pop-back, .pop-forward').css({'display': 'none'});
          $('.info').css({'display': 'block'});
          $('.arrows').css({'margin-left': '5% !important'});

        } else {

          $('.pop-back, .pop-forward').css({'display': 'block'});
          $('.info').css({'display': 'block'});
          $('.arrows').css({'margin-left': '5% !important'});
        }


      });

      $(window).on('resize', function(){

        $('.loading').css({
          'margin-left': window.innerWidth/2 - 15,
          'margin-top': window.innerHeight/2 - 15,
        })

        for (i=0; i<= projects.length; i++){

          if (this.innerWidth >= 767){

            $('.image-thumb_'+[i]).css({
              'height': $(this).innerHeight() 
            });

            for (e=0; e <=3; e++){
              $('.col'+[e]).css({'height': $(this).innerHeight() });
              $('.container').css({'width': $(this).innerWidth() });
            }

            colWidth = this.innerHeight

          } else {

            $('.image-thumb_'+[i]).css({
              'height': $('.inside2').innerHeight(),
              'width': (100/projects.length).toFixed(5)+'%'
            });

            $('.inside2').css({'width': (projects.length*100)+'%'});

            colWidth = $('.inside2').width()/projects.length;

          }

        }

        $(".col2").touchwipe({

          wipeLeft: function() {    

            $('.col2').animate({
              scrollLeft: $('.col2').scrollLeft() + colWidth
            },{
                duration: 90
            });

          },

          wipeRight: function() {         

            $('.col2').animate({
              scrollLeft: $('.col2').scrollLeft() - colWidth
            },{
                duration: 90     
            });

          },

          min_move_y: 20,
          preventDefaultEvents: true

        });

      }).trigger('resize');

  });

  // Buttons ===================================================== 

  $('.work-row > div').click(function(){
    $('.dim').addClass('toggle');
    $('.close-image').addClass('toggle');
    $('.pop').addClass('toggle');
    $('.poptext').addClass('toggle');
    $('.cvback').fadeOut('fast');
    $('.pop-forward').addClass('toggle');
    $('.pop-back').addClass('toggle'); 
    $('.arrows').addClass('toggle'); 
  });

  $('.close-image').click(function(){
    $('.dim').removeClass('toggle');
    $(this).removeClass('toggle');
    $('.pop').removeClass('toggle');
    $('.poptext').removeClass('toggle');
    $('.cvback').fadeIn('slow');
    $('.pop-forward').removeClass('toggle');
    $('.pop-back').removeClass('toggle'); 
    $('.arrows').removeClass('toggle'); 
    $('.info-dim').removeClass('toggle');
    $('.loading').fadeTo(300, 0);

    setTimeout(function(){ 
      $('.pop').css({'background-image': 'none'});
    }, 300);

  });

  $('.info').click(function(){
    $('.info-dim').toggleClass('toggle');
  });

  // Ajax =========================================================== 

  var ajaxFunc = {

    init: function(){
      this.getPage('views/home.html');
      this.bindEvents();
    },

    bindEvents: function(){
      $('.ajax-link').on('click', function(e){
        ajaxFunc.getPage($(this).attr('href'), true);
        e.preventDefault();
      });
    },

    getPage: function(url, anim){
      $.ajax({
        'url' : url,
        'success' : function(html){

          if(!anim){
            $('.inside1').html(html);
            ajaxFunc.bindEvents();
            return false;
          }

          $('.inside1').slideUp(200, function(){
            $('.inside1').html(html).slideDown(200);
            ajaxFunc.bindEvents();
          });

        }
      });
    }

  };

  ajaxFunc.init();

})();